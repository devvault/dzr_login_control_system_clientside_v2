class CfgPatches
{
	class dzr_login_control_system_clientside_v2
	{
		requiredAddons[] = {"DZ_Data"};
		units[] = {};
		weapons[] = {};
	};
};

class CfgMods
{
	class dzr_login_control_system_clientside_v2
	{
		type = "mod";
		author = "dayzrussia.com";
		description = "dzr_login_control_system_clientside_v2";
		dir = "dzr_login_control_system_clientside_v2";
		name = "dzr_login_control_system_clientside_v2";
		inputs = "dzr_login_control_system_clientside_v2/Data/Inputs.xml";
		dependencies[] = {"Game", "World", "Mission"};
		class defs
		{
			class gameScriptModule
			{
				files[] = {"dzr_login_control_system_clientside_v2/3_Game"};
			};
			class worldScriptModule
			{
				files[] = {"dzr_login_control_system_clientside_v2/4_World"};
			};
			class missionScriptModule
			{
				files[] = {"dzr_login_control_system_clientside_v2/5_Mission"};
			};

		};
	};
};