modded class PlayerBase
{
	int m_Validated = 1;
	void PlayerBase()
	{	
		GetRPCManager().AddRPC( "DZR_LCS_RPC", "OnPlayerLoadedValidated", this, SingleplayerExecutionType.Both );
	
	}//---------------------------------------------------------------------------------------------------------------------------
	
	

	void OnPlayerLoadedValidated(CallType type, ref ParamsReadContext ctx, ref PlayerIdentity sender, ref Object target)
	{
		//Print("OnPlayerLoadedValidated CALLED");
		ref Param1<int> data;
		if ( !ctx.Read( data ) )
		{
			//Print("OnPlayerLoadedValidated !ctx.Read( data )");
			return;	
		};
		if (type == CallType.Client)
        {
            if (data.param1 == 2)
            {
				//Print("Call OnPlayerLoaded(2);");
				m_Validated = 2;
				OnPlayerLoaded();
			}
            if (data.param1 == 3)
            {
				m_Validated = 3;
				ref Param1<PlayerBase> m_Data = new Param1<PlayerBase>(this);
				GetRPCManager().SendRPC( "DZR_LCS_RPC", "KickMe", m_Data, true, GetIdentity());
			}
		}
	}
	
	override void OnPlayerLoaded()
	{
		//Print("Call OnPlayerLoaded(); m_Validated= "+m_Validated);
		//send status to server, identity, player
		if(m_Validated == 1)
		{
			ref Param1<PlayerBase> m_Data = new Param1<PlayerBase>(this);	
			GetRPCManager().SendRPC( "DZR_LCS_RPC", "LoadedPlayerStatus", m_Data, true, GetIdentity());
		}
		if(m_Validated == 2)
		{
			super.OnPlayerLoaded();
		}
		
		
	}
	
	override void SetNewCharName()
	{
		////Print("SetNewCharName");
		g_Game.GetMenuData().SaveCharacter(false,true);
		g_Game.GetMenuData().SetCharacterName(g_Game.GetMenuData().GetLastPlayedCharacter(), g_Game.GetMenuDefaultCharacterData(false).GetCharacterName() );
		//g_Game.GetMenuData().SaveCharacter(false,true);
		g_Game.GetMenuData().SaveCharactersLocal();
		//Print("NAME: "+g_Game.GetMenuDefaultCharacterData(false).GetCharacterName());
	};
	
}	